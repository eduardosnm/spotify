<?php

return [
    "spotify" => [
        "base_uri" => env('SPOTIFY_BASE_URI'),
        "client_id" => env('SPOTIFY_CLIENT_ID'),
        "secret" => env('SPOTIFY_SECRET'),
        "auth_uri" => env('SPOTIFY_AUTHORIZATION_URI'),
        "encoded" => "Basic " . base64_encode(env('SPOTIFY_CLIENT_ID').":".env('SPOTIFY_SECRET'))
    ]
];
