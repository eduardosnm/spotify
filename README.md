# Challenge Spotify

Esta api tiene un endpoint en el cual se ingresa el nombre de una banda y se obtiene toda la discografia desde la api 
de spotify


- [Instalación](#instalacion)
- [Configuración](#configuracion)
- [Ejecutar](#ejecutar)
- [Endpoints](#endpoints)

#### Instalacion
Instalar redis en UBUNTU desde la terminal `sudo apt install redis`.

Instalar redis en WINDOWS.
> https://github.com/MicrosoftArchive/redis

Instalar redis en MAC OS X
> https://redis.io/download

Abrir la terminal y clonar el repositorio.
> git clone https://eduardosnm@bitbucket.org/eduardosnm/spotify.git

Desde la terminal ingresar a la carpeta spotify `cd spotify` y luego 
instalar dependencias con el gestor [composer](https://getcomposer.org/)

`composer install`

Hacer una copia del archivo .env.example `cp .env.example .env`


#### Configuración
Con un editor de codigo abrir el archivo .env y editar las siguientes lineas:
>SPOTIFY_CLIENT_ID=
>
>SPOTIFY_SECRET=

Para obtener estos valores hay que crear una nueva app en el [dashboard](https://developer.spotify.com/dashboard/applications) de spotify.
Una vez creada nos otorgará un Client ID y un Client Secret que deben ser asignados a las variables mencionadas arriba.

#### Ejecutación
Desde la consola, ingresar a la raiz de la carpeta del proyecto y ejecutar el siguiente comando:
`php -S localhost:8000 -t ./public`

#### Endpoints

> http://localhost:8000/api/v1/albums

##### Parametros
| Parametro      | Valor | 
| :---        |    :----:   |
| q      | Obligatorio. Si el nombre tiene mas de una palabra estas deben ir separadas por un guion medio (-)       |
| page   | Opcional. Campo numerico que indica el numero de pagina      |

Ejemplo: `http://localhost:8000/api/v1/albums?q=<band-name>&page=1
`

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
