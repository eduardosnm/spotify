<?php

namespace App\Http\Controllers;

use App\Resources\AlbumResource;
use App\Services\ConsumeApi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;


class AlbumController extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            "q" => "required|regex:/^[a-z0-9][-a-z0-9]*$/",
            "page" => "nullable|numeric|min:1"
        ]);

        if (Cache::has($request->server('QUERY_STRING'))) {
            return Cache::get($request->server('QUERY_STRING'));
        }

        $artist = urlencode($request->q);
        $page = $request->page ?? 1;
        $api = new ConsumeApi($artist, $page);
        $albums = $api->getAlbums();

        if (empty($albums)) {
            return response(["error" => "No data found", "code" => Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);
        }

        $collection =  AlbumResource::collection($albums["items"])
            ->additional(["meta" => $this->paginationMeta($albums["total"], $request)]);

        Cache::put($request->server('QUERY_STRING'), $collection, 300);

        return $collection;
    }

    private function paginationMeta($total, $request)
    {
        $artist_slug = $request->q;
        $last_page = ceil($total / ConsumeApi::RECORDS_PER_PAGE);
        $prev = $request->page > 1 ? $request->page - 1 : null;
        $next = $request->page < $last_page ? $request->page + 1 : null;

        return [
            "current_page" => $request->page ?? 1,
            "first_page_url" => env('APP_URL')."?q={$artist_slug}&page=1",
            "from" => 1,
            "last_page" => $last_page,
            "last_page_url" => env('APP_URL')."?q={$artist_slug}&page={$last_page}",
            "next_page_url" => !is_null($next) ? env('APP_URL')."?q={$artist_slug}&page={$next}" : null,
            "path" => env('APP_URL')."?q={$artist_slug}",
            "per_page" => 20,
            "prev_page_url" => !is_null($prev) ? env('APP_URL')."?q={$artist_slug}&page={$prev}" : null,
            "total" => $total
        ];
    }

}
