<?php


namespace App\Resources;


use Illuminate\Http\Resources\Json\ResourceCollection;

class AlbumResource extends ResourceCollection
{
    private $data;

    public function __construct($resource, $data)
    {
        parent::__construct($resource);
        $this->data = $data;
    }

    public function toArray($request)
    {
        return [
            "name" => $this->collection["name"] ?? '',
            "released" => $this->collection["release_date"] ? date("d-m-Y", strtotime($this->collection["release_date"])) : '',
            "tracks" => $this->collection["total_tracks"] ?? '',
            "cover" => $this->collection["images"][0] ?? '',
        ];
    }


}
