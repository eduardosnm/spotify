<?php


namespace App\Services;

use Illuminate\Support\Facades\Http;

class ConsumeApi extends Authorization
{
    private $artist;
    private $uri;
    private $offset;

    CONST RECORDS_PER_PAGE = 20;

    public function __construct(string $artist, int $page = 1)
    {
        $this->artist = $artist;
        $this->uri = config('services.spotify.base_uri');
        $this->offset = ($page - 1) * self::RECORDS_PER_PAGE;
    }

    public function getAlbums()
    {
        $albums_uri = $this->getArtistId();

        if(!$albums_uri) {
            return [];
        }

        $query = http_build_query([
            "offset" => $this->offset,
            "limit" => self::RECORDS_PER_PAGE,
            "include_groups" => "album"
        ]);

        return Http::withToken($this->getToken())
            ->get($albums_uri."/albums?" . $query)
            ->throw()
            ->json();
    }

    private function getArtistId()
    {
        $artist = Http::withToken($this->getToken())
            ->get($this->uri."/search?q={$this->artist}&type=artist")
            ->throw()
            ->json();

        if(!isset($artist["artists"]["items"][0])){
            return [];
        }

        return $artist["artists"]["items"][0]["href"];
    }

}
