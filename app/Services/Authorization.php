<?php


namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class Authorization
{

    private function authorize()
    {
        $response =  Http::withHeaders(["Authorization" => config('services.spotify.encoded')])
            ->asForm()->post(config('services.spotify.auth_uri'), [
                "grant_type" => "client_credentials",
            ])->throw()->json();

        Cache::put("access_token",$response['access_token'], 10);
    }

    public function getToken()
    {
        if (!Cache::has("access_token")) {
            $this->authorize();
        }

        return Cache::get("access_token");
    }

}
