<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof HttpException) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
            return \response()->json(['error' => $message, 'code' => $code], $code);
        }

        if ($exception instanceof  ValidationException) {
            $errors = $exception->validator->errors()->getMessages();
            return \response()->json(['error' => $errors, 'code' => Response::HTTP_UNPROCESSABLE_ENTITY], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($exception instanceof \InvalidArgumentException) {
            return \response()->json(['error' => "No data found", 'code' => 404], Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof Exception) {
            $code = $exception->getCode();
            $message = $exception->getMessage();
            return \response()->json(['error' => $message, 'code' => $code], $code);
        }

        if (env('APP_DEBUG', false)) {
            return parent::render($request, $exception);
        }

        return \response()->json('Unexpected error please try again later', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
